#!/bin/bash

RUNARGS="Usage: \n sudo sh installStrongswan.sh \nRun this script with sudo permissions."

if [ `id -u` -eq 0 ]
then
	echo "[ ] Installing latest EPEL-RELEASE"
	/usr/bin/yum -y install http://ftp.nluug.nl/pub/os/Linux/distr/fedora-epel/7/x86_64/e/epel-release-7-9.noarch.rpm
	/usr/bin/yum -y update
	echo "[ ] Installing openSwan and openssl"
	/usr/bin/yum -y install strongswan openssl

	cd ~
	cd ~/strongswan
	/usr/bin/mv ~/strongswan/server_key.sh /etc/strongswan/ipsec.d/server_key.sh
	/usr/bin/mv ~/strongswan/client_key.sh /etc/strongswan/ipsec.d/client_key.sh

	cd /etc/strongswan/ipsec.d
	/usr/bin/chmod a+x server_key.sh
	/usr/bin/chmod a+x client_key.sh

	echo "[?] Provide the external IP Address for this server and press [ENTER]: "
	read EXTERNALIP

	#generate certificate for the server
	/bin/bash /etc/strongswan/ipsec.d/server_key.sh $EXTERNALIP

	echo "[ ] Configuring stronswan"
	echo "config setup
    uniqueids=never
    charondebug=\"cfg 2, dmn 2, ike 2, net 0\"

conn %default
    left=%defaultroute
    leftsubnet=0.0.0.0/0
    leftcert=vpnHostCert.pem
    right=%any
    rightsourceip=172.16.1.100/16

conn CiscoIPSec
    keyexchange=ikev1
    fragmentation=yes
    rightauth=pubkey
    rightauth2=xauth
    leftsendcert=always
    rekey=no
    auto=add

conn XauthPsk
    keyexchange=ikev1
    leftauth=psk
    rightauth=psk
    rightauth2=xauth
    auto=add

conn IpsecIKEv2
    keyexchange=ikev2
    leftauth=pubkey
    rightauth=pubkey
    leftsendcert=always
    auto=add

conn IpsecIKEv2-EAP
    keyexchange=ikev2
    ike=aes256-sha1-modp1024!
    rekey=no
    leftauth=pubkey
    leftsendcert=always
    rightauth=eap-mschapv2
    eap_identity=%any
    auto=add" > /etc/strongswan/ipsec.conf

	echo "charon {
    load_modular = yes
    duplicheck.enable = no
    compress = yes
    plugins {
            include strongswan.d/charon/*.conf
    }
    dns1 = 8.8.8.8
    dns2 = 8.8.4.4
    nbns1 = 8.8.8.8
    nbns2 = 8.8.4.4
}

include strongswan.d/*.conf" > /etc/strongswan/strongswan.conf

	echo "[ ] Configurating IPv4 Forwarding"
	echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
	sysctl -p

	echo "[ ] Configurating CentOS Firewall"
	/usr/bin/firewall-cmd --permanent --add-service="ipsec"
	/usr/bin/firewall-cmd --permanent --add-port=4500/udp
	/usr/bin/firewall-cmd --permanent --add-masquerade
	/usr/bin/firewall-cmd --reload

	echo "[ ] Setup the first user for VPN\nPlease enter a username (no spaces or special characters allowed) and press [ENTER] : "
	read USERNAME
	echo "[?] Enter a password for this user and press [ENTER] : "
	read PASSWORD

	echo ": RSA vpnHostKey.pem
: PSK "PSK_KEY"
$USERNAME %any : EAP \"$PASSWORD\"
$USERNAME %any : XAUTH \"$PASSWORD\"" > /etc/strongswan/ipsec.secrets

	echo "[ ] Generating VPN client profile, the to be questioned password is to install the certificate on the client(s)!"
	echo "[?] Enter a name (no spaces!) to 'define' your VPN server and press [ENTER] : "
	read VPNNAME
	echo "[?] Enter an emailadress to 'define' your VPN server and press [ENTER] : "
	read EMAIL
	/usr/bin/bash /etc/strongswan/ipsec.d/client_key.sh $VPNNAME $EMAIL

	echo "[ ] Starting VPN server"
/usr/bin/systemctl start strongswan
/usr/bin/systemctl enable strongswan

	echo "[!] Send '/etc/strongswan/ipsec.d/cacerts/strongswanCert.pem' and '/etc/strongswan/ipsec.d/$VPNNAME.p12' by email to the client. Devices"

else
	echo $RUNARGS
	exit 1
fi
exit 1
