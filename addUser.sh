#!/bin/bash
hash strongswan 2>/dev/null || { echo >&2 "[!] strongSwan is not installed yet! Aborting."; exit 1; }

echo "[ ] Adding user(s) to your VPN server\nPlease enter a username (no spaces or special characters allowed) and press [ENTER] : "
read USERNAME
echo "[?] Enter a password for this user and press [ENTER] : "
read PASSWORD

echo "$USERNAME %any : EAP \"$PASSWORD\"
$USERNAME %any : XAUTH \"$PASSWORD\"" >> /etc/strongswan/ipsec.secrets
echo "[ ] Restarting VPN server"
/usr/sbin/service strongswan restart
